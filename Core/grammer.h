#ifndef GRAMMER_H
#define GRAMMER_H

#include <QObject>
#include <QQmlEngine>

class Grammer : public QObject
{
    Q_OBJECT

public:
    // Default constructor, required for classes you expose to QML.
    Grammer() : QObject() {}

    enum Article
    {
        Unknown = Qt::UserRole + 1,
        Masculine,
        Feminine,
        Neuter,
        Plural
    };

    Q_ENUMS(EnStyle)
};
#endif // GRAMMER_H
