#ifndef WORD_H
#define WORD_H

#include "grammer.h"

#include <QString>
#include <QVector>

class Word
{
public:
    Word();
    Word(const QString& word, Grammer::Article article = Grammer::Article::Unknown);
    Word(std::shared_ptr<QString> word,
         const QVector<std::shared_ptr<QString>>& relatives,
         Grammer::Article article = Grammer::Article::Unknown);
    Word(std::shared_ptr<QString> word,
         std::shared_ptr<QString> pluralCase,
         const QVector<std::shared_ptr<QString>>& relatives,
         Grammer::Article article = Grammer::Article::Unknown);

    Grammer::Article article() const;
    void setArticle(const Grammer::Article &article);

    QVector<std::shared_ptr<QString>> relatives() const;
    void setRelatives(const QVector<std::shared_ptr<QString>> &relatives);

    std::shared_ptr<QString> word() const;
    void setWord(const std::shared_ptr<QString> &word);

    std::shared_ptr<QString> pluralCase() const;
    void setPluralCase(const std::shared_ptr<QString> &pluralCase);

private:
    std::shared_ptr<QString> m_word;
    std::shared_ptr<QString> m_pluralCase;
    QVector<std::shared_ptr<QString>> m_relatives;
    Grammer::Article m_article;
};
#endif // WORD_H
