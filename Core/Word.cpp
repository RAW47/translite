#include "Word.h"

Word::Word()
{}

Word::Word(const QString &word, Grammer::Article article): m_word(std::make_shared<QString>(word)),
                                                  m_article(article){}

Word::Word(std::shared_ptr<QString> word, const QVector<std::shared_ptr<QString> > &relatives,
           Grammer::Article article) : m_word(word),
                              m_relatives(relatives),
                              m_article(article){}

Word::Word(std::shared_ptr<QString> word, std::shared_ptr<QString> pluralCase,
           const QVector<std::shared_ptr<QString> > &relatives,
           Grammer::Article article) : m_word(word),
                              m_pluralCase(pluralCase),
                              m_relatives(relatives),
                              m_article(article){}

Grammer::Article Word::article() const
{
    return m_article;
}

void Word::setArticle(const Grammer::Article &article)
{
    m_article = article;
}

QVector<std::shared_ptr<QString> > Word::relatives() const
{
    return m_relatives;
}

void Word::setRelatives(const QVector<std::shared_ptr<QString> > &relatives)
{
    m_relatives = relatives;
}

std::shared_ptr<QString> Word::word() const
{
    return m_word;
}

void Word::setWord(const std::shared_ptr<QString> &word)
{
    m_word = word;
}

std::shared_ptr<QString> Word::pluralCase() const
{
    return m_pluralCase;
}

void Word::setPluralCase(const std::shared_ptr<QString> &pluralCase)
{
    m_pluralCase = pluralCase;
}

