import QtQuick 2.9
import QtQuick.Controls 2.2

ApplicationWindow
{
    id: mainWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Scroll")

    readonly property real smallSideMargin: 5

    Rectangle
    {
        id:rectTop
        anchors.top: mainWindow.top
        anchors.left: mainWindow.left
        anchors.leftMargin: smallSideMargin
        height: mainWindow.height / 8
        width: mainWindow.width
        border.color: "black"

        TextField
        {
            id: txtArea
            anchors.fill: parent
            anchors.margins: smallSideMargin
            font.pixelSize: mainWindow.height / 20
            selectByMouse: true
        }
    }
    Rectangle
    {
        id:rectTopLeft
        anchors.top: rectTop.bottom
        anchors.left: mainWindow.left
        anchors.margins: smallSideMargin
        height: mainWindow.height/4
        width: mainWindow.width/2
        border.color: "black"

        ComboBox
        {
            id: comboLang
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: smallSideMargin
            height: parent.height/4
            model: ["English", "Deutsch", "Kurdish"]
        }
        ComboBox
        {
            id: comboType
            anchors.top: comboLang.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: smallSideMargin
            height: comboLang.height
            model: ["Noun", "Verb", "Adjective"]
        }
        ComboBox
        {
            id: comboArticle
            anchors.top: comboType.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: smallSideMargin
            height: comboLang.height
            model: ListModel
            {
                ListElement { key: "Unknown"; value: 0 }
                ListElement { key: "Der"; value: 1 }
                ListElement { key: "Die"; value: 2 }
                ListElement { key: "Das"; value: 3 }
                ListElement { key: "Diee"; value: 4 }
            }
        }
    }
    Rectangle
    {
        id:rectTopRight
        anchors.top: rectTop.bottom
        anchors.left: rectTopLeft.right
        anchors.right: rectTop.right
        anchors.margins: smallSideMargin
        height: rectTopLeft.height
        border.color: "black"
    }

//    ScrollView
//    {
//        anchors.top: rectTop2.bottom
//        width: mainWindow.width
//        anchors.bottom: mainWindow.bottom

//        ListView
//        {
//            width: parent.width
//            model: 20
//            delegate: ItemDelegate
//            {
//                text: "Item " + (index + 1)
//                width: parent.width
//            }
//        }
//    }
}
