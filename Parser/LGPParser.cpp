#include "LGPParser.h"

#include <regex>

LGPParser::ProcessType getProcessType(std::string str)
{
    if (str.find("LGP=") != std::string::npos)
    {
       return LGPParser::ProcessType::Language;
    }
    else if (str.find_first_of("<") != std::string::npos &&
             str[str.find_first_of("<")+1] != '/')
    {
        if (str.find("<\\") != std::string::npos)
        {
            return LGPParser::ProcessType::OneLineProcess;
        }
        else
        {
            return LGPParser::ProcessType::Opening;
        }
    }
    else if (1)
    {

    }
//    if (str.find_first_of("<\\") != std::string::npos)
//    {
//        //TODO add more checkers for  the processing types
//        //using Regex would be better
//       return LGPParser::ProcessType::Language;
//    }
//    if (str.find("LGP=") != std::string::npos)
//    {
//       return LGPParser::ProcessType::Language;
//    }
    return LGPParser::ProcessType::Unknown;
}

void LGPParser::startParsing()
{
    std::fstream file(m_filePath, std::ios_base::out);
    std::string curentLine;
    //std::regex regex;

    while(!file.eof())
    {
        std::getline(file, curentLine);
        switch(getProcessType(curentLine))
        {
        case LGPParser::ProcessType::Opening:
            break;
        case LGPParser::ProcessType::Closing:
            break;
        case LGPParser::ProcessType::Language:
            break;
        case LGPParser::ProcessType::Processing:
            break;
        default:
            break;

        }



    }
}
