#ifndef LGPPARSER_H
#define LGPPARSER_H

#include <string>
#include <fstream>

class LGPParser
{
public:
    void startParsing();

    enum ProcessType
    {
        Unknown = 0,
        Processing,
        Language,
        Opening,
        Closing,
        OneLineProcess,
        Word,
        Attribute
    };

private:
    std::string m_filePath;
};

#endif // LGPPARSER_H
